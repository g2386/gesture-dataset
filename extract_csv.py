import os
import json
import numpy as np

from argparse import ArgumentParser

POSE = 'pose'
GEST = 'gesture'

POSE_CLS = ['PTR', 'SCR', 'STP']
GEST_CLS = ['PRESS', 'RELEASE', 'CLICK', 'DOUBLE', 'RIGHT', 'ZIN', 'ZOUT']


def filter_pose(fname):
    return any([cls in fname for cls in POSE_CLS])


def filter_gest(fname):
    return True


def get_landmarks(path):
    with open(path) as f:
        return json.load(f)


def frame2np(frame):
    return np.array([[lm[c] for lm in frame] for c in "xyz"])


def norm(frame):
    m = np.min(frame, axis=1, keepdims=True)
    M = np.max(frame, axis=1, keepdims=True)
    return (frame - m) / (M - m)


def get_csv_header(frame_count):
    header = ['gesture']
    long_cols = frame_count > 1
    for iframe in range(frame_count):
        for c in "xyz":
            for lm in range(21):
                header.append(f"{c}{lm}_{iframe}" if long_cols else f"{c}{lm}")
    return ','.join(header) + '\n'


def process_pose(clazz, landmarks):
    lines = []
    for frame in landmarks:
        np_frame = norm(frame2np(frame)).flatten()
        cols = [clazz] + [str(x) for x in np_frame]
        lines.append(cols)
    return lines


def process_gest(clazz, landmarks):
    if clazz in GEST_CLS:
        cols = [clazz]
        frames = landmarks
    else:
        cols = ["NONE"]
        lm_count = len(landmarks)
        extract_size = np.random.randint(30, min(61, lm_count))
        start_point = np.random.randint(0, lm_count - extract_size)
        frames = landmarks[start_point:start_point+extract_size]
    for frame in frames:
        np_frame = norm(frame2np(frame)).flatten()
        cols += [str(x) for x in np_frame]
    return [cols]


def postproc_pose(rows, max_frames):
    return list(map(lambda row: ','.join(row) + '\n', rows))


def postproc_gest(rows, max_frames):
    for i in range(len(rows)):
        frame_count = (len(rows[i]) - 1) // 63
        if frame_count < max_frames:
            rows[i] += ["0.0" for _ in range(63 * (max_frames - frame_count))]
    return postproc_pose(rows, max_frames)


def main(args):
    gtype = args.variant

    file_filter = filter_pose if gtype == POSE else filter_gest
    process = process_pose if gtype == POSE else process_gest
    postproc = postproc_pose if gtype == POSE else postproc_gest

    rows = []
    max_frames = 1
    examples = filter(file_filter, os.listdir(args.source))

    for ex in examples:
        gesture = ex.split("_")[1]
        landmarks = get_landmarks(os.path.join(args.source, ex))
        if gtype == GEST and gesture in GEST_CLS:
            max_frames = max(max_frames, len(landmarks))
        rows += process(gesture, landmarks)

    csv = postproc(rows, max_frames)
    header = get_csv_header(max_frames)

    with open(args.dest, 'w') as dest:
        dest.write(header)
        dest.writelines(csv)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("variant", type=str, help="gesture type to extract", choices=[POSE, GEST])
    parser.add_argument("source", type=str, help="root directory of all JSON files")
    parser.add_argument("dest", type=str, help="path to destination file")
    args = parser.parse_args()
    main(args)
